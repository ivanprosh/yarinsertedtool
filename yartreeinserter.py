import sys
import argparse
import os
import io
import re
import xml.etree.ElementTree
from xml.sax.saxutils import escape as xml_escape

__message = str()

class WrongFileException(Exception):
    pass

#   pattern - рег. выражение, по которуму определяем, подходит ли файл для вставки
def DirAsXML(path, relativePath, suffix, pattern):
    try:
        result = '<group>\n <name>%s</name>\n' % xml_escape(os.path.basename(path))
        dirs = []
        files = []
        for item in os.listdir(path):
            itempath = os.path.join(path, item)
            if os.path.isdir(itempath):
                dirs.append(item)
            elif os.path.isfile(itempath) and pattern.match(item.lower()):
                files.append(suffix + os.path.relpath(itempath,relativePath))
        if files:
            result += '\n'.join('    <file>\n      <name>%s</name>\n    </file>'
                % xml_escape(f) for f in files) + '\n'
        if dirs:
            for d in dirs:
                x = DirAsXML(os.path.join(path, d), relativePath, suffix, pattern)
                result += '\n'.join(' ' + line for line in x.split('\n'))
        result += '\n</group>'
    except FileNotFoundError as err:
        print(str(err))
        return None
    return result


def parseTree(filename):
    global __message
    tags = set()
    tree = None
    fh = None
    try:
        with open(filename, encoding="utf8",
                  errors="ignore") as fh:
            line = fh.readline()
            if not line.startswith("<?xml"):
                raise WrongFileException()
            fh.close()
        tree = xml.etree.ElementTree.parse(filename)
    except WrongFileException:
        __message = "check xml format, wrong first row"
    except FileNotFoundError as err:
        __message = str(err)
    except xml.etree.ElementTree.ParseError as err:  # Python 3.2
        __message = "has the following error:\n    " + str(err)
    except xml.parsers.expat.ExpatError as err:
        __message = "has the following error:\n    " + str(err)
    except EnvironmentError as err:
        __message = "could not be read:\n    " + str(err)
    else:
        #__message = ("uses the following tags:\n    " +
        #           "\n    ".join(sorted(tags, key=str.lower)))

        __message = (filename + " is an XML file ") #+  __message)
    print(__message)
    return tree


def findParentItemByChildTagName(tree, name):
    root = tree.getroot()
    #Генератор списка элементов дерева, работает следующим образом:
    #XPath = "./group//name/.."
    #возвращает все элементы любой вложенности дерева, имеющих среди родительских узлов тег - group
    #а среди дочерних тег - name
    #Затем согласно условию проверяется, что найденные теги относятся к группе тегов group, чтобы
    #исключить попадание тегов с именем file.
    #Из полученных тегов в результирующий список попадают все теги, имеющие в качестве дочернего тега итем с целевым именем
    elements = [el for el in root.findall("./group//name/..") if el.tag == 'group']
    if name not in (el.find('name').text for el in elements):
        print('{0} not found in tree folders'.format(name))
        return None
    return next(el for el in elements if el.find('name').text == name)

#for element in tree.iter():
#    tags.add(element.tag)
# root = tree.getroot()
# root.findall(".//year/..[@name='Singapore']")
# print(root.findall("./group//*"))
# texts = [el.text for el in root.findall("./group//name")]
# print(texts)

def main():
    parser = argparse.ArgumentParser(description='Insert XML subtree Folders and files for YAR Project')
    parser.add_argument('path_to_ewp', metavar='path_to_ewp', nargs=1,
                        help='Path to ewp project file')
    parser.add_argument('path_to_insert_root_folder', metavar='path_to_insert_root_folder', nargs=1,
                        help='Path to insert root folder')
    parser.add_argument('goal_name', metavar='goal_name', nargs=1,
                        help='Name folder, that must be parent in project')
    parser.add_argument('--file-patterns', type=str, default='.*', metavar='file_patterns',
                        help='RegExp for inserted files, for ex: ".*\.(cpp|h)$"')
    args = parser.parse_args()

    goal_name = args.goal_name[0]#'PGPA_IN'
    path_to_insert_root_folder = args.path_to_insert_root_folder[0]#'H:\WORK\_AutomationProjects\Test\MCU\GPA1\PGPA_IN\TEST_CONFIG'
    path_to_ewp = args.path_to_ewp[0]#'H:\WORK\_AutomationProjects\Test\MCU\Project\PK_GPA1.ewp'

    __pattern = re.compile(args.file_patterns.strip('\'"'))

    print(path_to_ewp, path_to_insert_root_folder, goal_name, __pattern)
    projectDir, filename = os.path.split(path_to_ewp)
    tree = parseTree(path_to_ewp)

    if(tree is not None):
        subElement = findParentItemByChildTagName(tree, goal_name)
        if(subElement is not None):
            #print(subElement.text)
            fh = io.StringIO(DirAsXML(path_to_insert_root_folder, projectDir,'$PROJ_DIR$\\', __pattern))
            #print(DirAsXML(path_to_insert_root_folder))
            insertedtree = xml.etree.ElementTree.ElementTree()
            insertedRoot = insertedtree.parse(fh)
            #создаем элемент для вставки
            insEl = xml.etree.ElementTree.SubElement(subElement, 'group')
            insEl.extend(insertedRoot)
            tree.write(path_to_ewp)
main()

